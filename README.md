## Git reference

### Don't do this **ever**
```
$git add <directory>
$git add .
```
Both add everything in the directory, or the current working directory. Handy, right ? Until you notice that that actually includes all your hidden files, that 1GB error log dump you had lying around. It actually can hurt your teammates, if you have your project settings for whatever IDE you use in this directory, now you **will** override theirs, and it will get nasty with merge conflicts. So please, don't do this. It's not impossible to recover from, but it's easily avoided.

#### Starting
```
$git config --global user.name "your account name"
$git config --global user.email "your account email"
```

Saving your password (use SSH, but if you must, and on your own risk, this is how to avoid typing it). Newer versions of git have a credential manager, YMMV.
```
$git config remote.origin.url https://username:password@<server>/username/example.git
```

```
$git clone <repourl> # Creates a directory with the same name as your repo.
```

Pushing to two repositories
```
git remote set-url --add --push origin url1.git   # New in latest git
git remote set-url --add --push origin url2.git   # New in latest git
```

#### Safe commands
```
$git status # View your current local state
$git fetch # Get all new information from remote without touching local branches
```

#### Adding a new (untracked file)
```
$git add <filename>
```
Avoid using adding folders, this will quickly clutter your repository

#### Committing with adding all tracked changes
```
$git commit -a
```
Adds all changes to tracked files to your commit, opens editor window for commit message.

Will not add new files. (This is usually the cause for "Forget new file" commit messages).

#### Switching branches
```
$git checkout <branchname>
```
Create and checkout
```
$git checkout -b <branchname>
```

#### Merging
Merge master into your own branch, then merge the merged branch into master. (Safest way you can merge)
```
$git checkout master
$git pull # make sure you have everything from remote
$git checkout <yourbranch>
$git merge master
$git commit -a # Only in case of merge conflict, AFTER you fix the merge conflict, look for <<<< and >>>> signs in the code, HEAD indicates your code, MASTER, well you know :)
$git push origin <yourbranch> # publish your changes
$git checkout master
$git pull # Yes, again, because you don't know what happened during your 8 hour merge conflict
$git merge <yourbranch>
# Fix merge if needed and commit merge
$git push origin master
```

#### Misc
```
$git stash # Save changes to tracked files without committing
$git stash apply # Load
$git log --graph # CLI overview of history
$git diff <branch or commitid> <branch or commitid> # Show line by line difference of all files between two commits or branches
```
